package org.jmaniquet.prototypes.crossdbtesting;

import static org.jmaniquet.prototypes.crossdbtesting.JdbcUtils.getLocalDateTime;
import static org.jmaniquet.prototypes.crossdbtesting.JdbcUtils.nullableLong;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class PeopleRowMapper implements RowMapper<People> {

	@Override
	public People mapRow(ResultSet rs, int rowNum) throws SQLException {
		Long id = nullableLong(rs, "people_id");
		LocalDateTime birthDate = getLocalDateTime(rs, "birth_date");
		String name = rs.getString("name");
		
		People p = new People();
		p.setPeopleId(id);
		p.setName(name);
		p.setBirthDate(birthDate);
		
		return p;
	}
}
