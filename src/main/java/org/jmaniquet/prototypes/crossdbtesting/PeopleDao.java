package org.jmaniquet.prototypes.crossdbtesting;

public interface PeopleDao {

	void insert(People user);
	
	void update(People user);
	
	People select(Long userId);
	
	void delete(Long userId);
}
