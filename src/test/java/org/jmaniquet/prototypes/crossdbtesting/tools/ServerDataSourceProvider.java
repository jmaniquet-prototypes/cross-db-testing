package org.jmaniquet.prototypes.crossdbtesting.tools;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

@ConditionalOnProperty(
		prefix = "spring.datasource",
		name = {"url", "driver-class-name", "username"})
@Configuration
public class ServerDataSourceProvider implements DataSourceProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(ServerDataSourceProvider.class);

	@Value("${spring.datasource.url}")
	private String url;
	
	@Value("${spring.datasource.driver-class-name}")
	private String driverClassName;
	
	@Value("${spring.datasource.username}")
	private String username;
	
	@Autowired
	private Environment env;
	
	@Override
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setUrl(url);
		ds.setDriverClassName(driverClassName);
		ds.setUsername(username);
		
		String pwd = env.getProperty("spring.datasource.password");
		if (pwd != null) {
			ds.setPassword(pwd);
		}
		
		String script = "init-db-schema.sql";
		String dbProfile = env.getProperty("spring.datasource.db-profile");
		if (dbProfile != null) {
			script = "init-db-schema-" + dbProfile + ".sql";
		}
		
		logger.info("Initializing server datasource - [url={}][driver={}][user={}][script={}]", url, driverClassName, username, script);
		
		Resource res = new ClassPathResource(script);
		ResourceDatabasePopulator dp = new ResourceDatabasePopulator(res);
		DatabasePopulatorUtils.execute(dp, ds);
		
		return ds;
	}
}
