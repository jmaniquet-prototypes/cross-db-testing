package org.jmaniquet.prototypes.crossdbtesting.tools;

import javax.sql.DataSource;

public interface DataSourceProvider {

	DataSource dataSource();
}
