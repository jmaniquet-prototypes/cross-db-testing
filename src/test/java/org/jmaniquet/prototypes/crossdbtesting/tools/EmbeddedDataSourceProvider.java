package org.jmaniquet.prototypes.crossdbtesting.tools;

import static org.springframework.util.StringUtils.hasText;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@ConditionalOnMissingBean(value = DataSourceProvider.class, ignored = EmbeddedDataSourceProvider.class)
@Configuration
public class EmbeddedDataSourceProvider implements DataSourceProvider {

	private static final Logger logger = LoggerFactory.getLogger(EmbeddedDataSourceProvider.class);
	
	@Autowired
	private Environment env;
	
	@Override
	@Bean
	public DataSource dataSource() {
		String embeddedType = env.getProperty("spring.datasource.embedded-type");
		if (hasText(embeddedType)) {
			logger.info("Initializing embedded datasource - type = {}", embeddedType);
			EmbeddedDatabaseType type = EmbeddedDatabaseType.valueOf(embeddedType);
			return dataSource(type);
		}
		
		logger.info("Initializing embedded datasource - property 'spring.datasource.embedded-type' not found - defaulting to HSQL", embeddedType);
		return dataSource(EmbeddedDatabaseType.HSQL);
	}

	private EmbeddedDatabase dataSource(EmbeddedDatabaseType type) {
		return new EmbeddedDatabaseBuilder().addScript("init-db-schema.sql").setType(type).build();
	}
}
